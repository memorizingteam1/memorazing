﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing
{
    public partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();
            NumberOfWordsNum.Value = Options.NrOfWords;
            TimePerWordNum.Value = Options.WordSpeed;
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            Options.NrOfWords = (int) NumberOfWordsNum.Value;
            Options.WordSpeed = (int)TimePerWordNum.Value;
            ExportOptionsToFile();
            this.Close();
        }

        private void ExportOptionsToFile()
        {
            string path = Environment.CurrentDirectory + @"\options.txt";
            FileStream stream = new FileStream(path, FileMode.Create , FileAccess.Write);
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            dictionary.Add("NrOfWords", Options.NrOfWords);
            dictionary.Add("WordSpeed", Options.WordSpeed);

            //serialiseren
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, dictionary);
            stream.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
