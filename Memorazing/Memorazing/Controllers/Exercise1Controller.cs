﻿using Memorazing.Exercises;
using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.Controllers
{
    class Exercise1Controller : MainController
    {
        private Exercise1Form exercise = new Exercise1Form();

        public Exercise1Controller()
        {
            Random rnd = new Random();
            int month = rnd.Next(0, DictioWords.Count);
            string randomCategorie = DictioWords.ElementAt(month).Key;
            List<Word> tryGetValueWords = new List<Word>();
            DictioWords.TryGetValue(randomCategorie, out tryGetValueWords);
            Words = tryGetValueWords;
            Exercise.ProcessWordInitialize(Words);

            StartShowingAllTheWords();
        }

        public Exercise1Form Exercise
        {
            get{ return exercise;}
            set{ exercise = value;}
        }

        public void ShowWindow()
        {
            Exercise.Show();
        }

        public async void StartShowingAllTheWords()
        {
            int nrOfWord = 0;
            Exercise.Update();
            foreach (Word w in Words)
            {
                if (nrOfWord < Options.NrOfWords)
                {
                    await Task.Delay(Options.WordSpeed * 1000);
                    Exercise.AddShowWordLbl(w.Title + " \r\n");
                    nrOfWord++;
                    Exercise.Update();
                }
            }
            await Task.Delay(Options.WordSpeed * 1000);
            Exercise.AfterExercise();
        }
    }
}
