﻿using Memorazing.Exercises;
using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.Controllers
{
    class Exercise3Controller : MainController
    {
        private Exercise3Form exercise = new Exercise3Form();

        public Exercise3Controller()
        {
            Random rnd = new Random(); ;
            int month = rnd.Next(0, DictioWords.Count);
            string randomCategorie = DictioWords.ElementAt(month).Key;
            List<Word> tryGetValueWords = new List<Word>();
            DictioWords.TryGetValue(randomCategorie, out tryGetValueWords);
            Words = tryGetValueWords;
            Exercise.ProcessWordInitialize(Words);

            FillImageWithSound();
        }

        public Exercise3Form Exercise
        {
            get { return exercise; }
            set { exercise = value; }
        }

        public async void FillImageWithSound()
        {
            List<PictureBox> Pictures = Exercise.Pictures;

            int pictureCount = 0;
            String path;
            int nrOfWord = 0;

            foreach (Word word in Words)
            {
                if (nrOfWord < Options.NrOfWords)
                {
                    await Task.Delay(Options.WordSpeed * 1000);
                    path = word.UrlPictures;
                    using (FileStream fs = new FileStream(path, FileMode.Open))
                    {
                        Bitmap image = new Bitmap(Image.FromStream(fs), 100, 100);
                        Exercise.InitializePictureImage(pictureCount, image);
                        Exercise.InitilializePictureSound(word.Title);
                        pictureCount++;
                        Exercise.Update();
                    }
                    nrOfWord++;
                }

            }
            await Task.Delay(Options.WordSpeed * 1000);
            Exercise.AfterExercise();
        }
        public void ShowWindow()
        {
            Exercise.Show();
        }

    }
}