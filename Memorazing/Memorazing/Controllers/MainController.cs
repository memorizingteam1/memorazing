﻿using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Memorazing.Controllers
{
     public class MainController
    {
        private List<Word> words = new List<Word>();
        private static Dictionary<string, List<Word>> dictioWords;
        
        public MainController()
        {
            ReadWordsFromFile();
            ReadOptionsFromFile();
        }

        public List<Word> Words
        {
            get;    set;
        }

        public static Dictionary<string, List<Word>> DictioWords
        {
            get { return dictioWords; }
            set { dictioWords = value; }
        }

        private void ReadWordsFromFile()
        {
            try
            {
                string path = Environment.CurrentDirectory + @"\listOfWords.txt";
                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                BinaryFormatter formatter = new BinaryFormatter();
                DictioWords = (Dictionary<string, List<Word>>)formatter.Deserialize(stream);

                stream.Close();
            }
            catch (Exception e) { }
        }

        private void ReadOptionsFromFile()
        {
            try
            {
                string path = Environment.CurrentDirectory + @"\options.txt";
                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                BinaryFormatter formatter = new BinaryFormatter();
                Dictionary<string, int> savedOptions = (Dictionary<string, int>)formatter.Deserialize(stream);
                if (savedOptions.ContainsKey("NrOfWords"))
                {
                    Options.NrOfWords = savedOptions["NrOfWords"];
                }
                if (savedOptions.ContainsKey("WordSpeed"))
                {
                    Options.WordSpeed = savedOptions["WordSpeed"];
                }
                stream.Close();
            }
            catch (Exception e) { }
        }

        
    }
}
