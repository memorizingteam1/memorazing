﻿using Memorazing.Models;
using Memorazing.WordsList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.Controllers 
{
    class ModifyListController : MainController
    {
        private ModifyListForm modifyList;

        public ModifyListController()
        {
            modifyList = new ModifyListForm();
            int i = 0;

            foreach (string s in DictioWords.Keys)
            {
                /*label1.Text += s;
                label1.Visible = false;*/
                modifyList.CreateButtons(s, i);
                i += 30;

                /*foreach (Word w  in DictioWords[s])
                {
                    label1.Text += w.Title + " ";
                    label1.Text += w.UrlPictures + " ";
                    label1.Text += "-----\n";
                }*/
            }
        }

        public void DeleteWord()
        {
            foreach (Word w in DictioWords[modifyList.Label1Text()])
            {
                File.Delete(w.UrlPictures);
            }
            DictioWords.Remove(modifyList.Label1Text());
            ListForms.ExportListToFile();
        }

        internal void Show()
        {
            modifyList.Show();
        }
    }
}
