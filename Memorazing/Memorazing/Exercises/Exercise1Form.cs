﻿using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Memorazing.soundex;

namespace Memorazing.Exercises
{
    public partial class Exercise1Form : Form
    {

        private int guessesMade = 0;
        private int guessedWords = 0;
        private List<String> alreadyGuessedWords = new List<string>();
        ProcessWord processWord;

        public Exercise1Form()
        {
            InitializeComponent();
        }

        public void ProcessWordInitialize(List<Word> words)
        {
            processWord = new ProcessWord(words);
        }

        public void AddShowWordLbl(string word)
        {
            ShowWordLbl.Text += word;
        }

        public void AfterExercise()
        {
            ShowWordLbl.Text = "";
            SubmitWordBtn.Visible = true;
            InputWordTextBox.Visible = true;
        }

        private void SubmitWordBtn_Click(object sender, EventArgs e)
        {
            try {
                string guessedWord = InputWordTextBox.Text.TrimStart().TrimEnd().ToLower();
                string output = guessedWord;
                bool wordIsInList = processWord.GuessedWordInList(guessedWord);
                if (wordIsInList)
                {
                    output += "----O";
                    guessedWords++;
                }
                else
                {
                    output += "----X";
                }

                guessedWordsLabel.Text += output + " \r\n";
                guessesMade++;
                if (guessesMade >= Options.NrOfWords) endForm();

            } catch (Exception ex) {
                ErrorLabel.Text = ex.Message;
            } 

        }

        private void endForm()
        {
            InputWordTextBox.Enabled = false;
            SubmitWordBtn.Enabled = false;
            resultLabel.Text = string.Format("You have guessed {0} out of {1} words", processWord.guessedWords, Options.NrOfWords);
            History.addHistoryItem(new HistoryItem(processWord.guessedWords, Options.NrOfWords, 1));
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
