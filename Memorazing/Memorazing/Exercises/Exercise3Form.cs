﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Memorazing.Models;
using System.Speech.Synthesis;
using System.IO;


namespace Memorazing.Exercises
{
    public partial class Exercise3Form : Form
    {
        private List<PictureBox> pictures;
        private int guessesMade = 0;
        private int guessedWords = 0;
        private List<String> alreadyGuessedWords = new List<string>();
        SpeechSynthesizer synth = new SpeechSynthesizer();
        ProcessWord processWord;

        public Exercise3Form()
        {
            InitializeComponent();
            pictures = new List<PictureBox>()
            {
            ShowPicture1,ShowPicture2,ShowPicture3,ShowPicture4,ShowPicture5,
            ShowPicture6,ShowPicture7,ShowPicture8,ShowPicture9,ShowPicture10,
            ShowPicture11,ShowPicture12,ShowPicture13,ShowPicture14,ShowPicture15,
            ShowPicture16,ShowPicture17,ShowPicture18,ShowPicture19,ShowPicture20
            };
        }
        public List<PictureBox> Pictures
        {
            get { return pictures; }
        }
        
        public void InitializePictureImage(int pictureCount, Bitmap img)
        {
            Pictures[pictureCount].Image = img;
        }

        public void InitilializePictureSound(string title)
        {
            synth.Speak(title);
        }

        public void ProcessWordInitialize(List<Word> words)
        {
            processWord = new ProcessWord(words);
        }

        public void AfterExercise()
        {
            for (int i = 0; i < Pictures.Count; i++)
            {
                Pictures[i].Image = null;
            }
            SubmitWordBtn.Visible = true;
            InputWordTextBox.Visible = true;
        }

        private void endForm()
        {
            InputWordTextBox.Enabled = false;
            SubmitWordBtn.Enabled = false;
            resultLabel.Text = string.Format("You have guessed {0} out of {1} words", guessedWords, Options.NrOfWords);
            History.addHistoryItem(new HistoryItem(processWord.guessedWords, Options.NrOfWords, 3));
        }


        private void ExitBtn_Click(object sender, EventArgs e)
        {
            synth.Volume = 0;
            this.Close();
        }

        private void SubmitWordBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string guessedWord = InputWordTextBox.Text.TrimStart().TrimEnd().ToLower();
                string output = guessedWord;
                bool wordIsInList = processWord.GuessedWordInList(guessedWord);
                if (wordIsInList)
                {
                    output += "----O";
                    guessedWords++;
                }
                else
                {
                    output += "----X";
                }

                guessedWordsLabel.Text += output + " \r\n";
                guessesMade++;
                if (guessesMade >= Options.NrOfWords) endForm();

            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
        }
    }
}
