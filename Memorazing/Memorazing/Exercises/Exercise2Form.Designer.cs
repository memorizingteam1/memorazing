﻿namespace Memorazing.Exercises
{
    partial class Exercise2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exercise2Form));
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.GuessedWordsLabel = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.ShowPicture1 = new System.Windows.Forms.PictureBox();
            this.ShowPicture4 = new System.Windows.Forms.PictureBox();
            this.ShowPicture15 = new System.Windows.Forms.PictureBox();
            this.ShowPicture9 = new System.Windows.Forms.PictureBox();
            this.ShowPicture10 = new System.Windows.Forms.PictureBox();
            this.ShowPicture18 = new System.Windows.Forms.PictureBox();
            this.ShowPicture16 = new System.Windows.Forms.PictureBox();
            this.ShowPicture6 = new System.Windows.Forms.PictureBox();
            this.ShowPicture12 = new System.Windows.Forms.PictureBox();
            this.ShowPicture2 = new System.Windows.Forms.PictureBox();
            this.ShowPicture5 = new System.Windows.Forms.PictureBox();
            this.ShowPicture13 = new System.Windows.Forms.PictureBox();
            this.ShowPicture7 = new System.Windows.Forms.PictureBox();
            this.ShowPicture14 = new System.Windows.Forms.PictureBox();
            this.ShowPicture8 = new System.Windows.Forms.PictureBox();
            this.ShowPicture19 = new System.Windows.Forms.PictureBox();
            this.ShowPicture20 = new System.Windows.Forms.PictureBox();
            this.ShowPicture17 = new System.Windows.Forms.PictureBox();
            this.ShowPicture3 = new System.Windows.Forms.PictureBox();
            this.ShowPicture11 = new System.Windows.Forms.PictureBox();
            this.TextLbl1 = new System.Windows.Forms.Label();
            this.TextLbl15 = new System.Windows.Forms.Label();
            this.TextLbl18 = new System.Windows.Forms.Label();
            this.TextLbl12 = new System.Windows.Forms.Label();
            this.TextLbl9 = new System.Windows.Forms.Label();
            this.TextLbl20 = new System.Windows.Forms.Label();
            this.TextLbl17 = new System.Windows.Forms.Label();
            this.TextLbl7 = new System.Windows.Forms.Label();
            this.TextLbl2 = new System.Windows.Forms.Label();
            this.TextLbl14 = new System.Windows.Forms.Label();
            this.TextLbl6 = new System.Windows.Forms.Label();
            this.TextLbl4 = new System.Windows.Forms.Label();
            this.TextLbl19 = new System.Windows.Forms.Label();
            this.TextLbl5 = new System.Windows.Forms.Label();
            this.TextLbl13 = new System.Windows.Forms.Label();
            this.TextLbl11 = new System.Windows.Forms.Label();
            this.TextLbl10 = new System.Windows.Forms.Label();
            this.TextLbl16 = new System.Windows.Forms.Label();
            this.TextLbl8 = new System.Windows.Forms.Label();
            this.TextLbl3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture11)).BeginInit();
            this.SuspendLayout();
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.BackColor = System.Drawing.Color.Orange;
            this.SubmitWordBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitWordBtn.ForeColor = System.Drawing.Color.White;
            this.SubmitWordBtn.Location = new System.Drawing.Point(945, 471);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(107, 40);
            this.SubmitWordBtn.TabIndex = 0;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = false;
            this.SubmitWordBtn.Visible = false;
            this.SubmitWordBtn.Click += new System.EventHandler(this.SubmitWordBtn_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Orange;
            this.ExitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.White;
            this.ExitBtn.Location = new System.Drawing.Point(1025, 527);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(107, 40);
            this.ExitBtn.TabIndex = 1;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(775, 483);
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(136, 20);
            this.InputWordTextBox.TabIndex = 2;
            this.InputWordTextBox.Visible = false;
            // 
            // GuessedWordsLabel
            // 
            this.GuessedWordsLabel.AutoSize = true;
            this.GuessedWordsLabel.BackColor = System.Drawing.Color.Transparent;
            this.GuessedWordsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuessedWordsLabel.ForeColor = System.Drawing.Color.Orange;
            this.GuessedWordsLabel.Location = new System.Drawing.Point(1004, 58);
            this.GuessedWordsLabel.Name = "GuessedWordsLabel";
            this.GuessedWordsLabel.Size = new System.Drawing.Size(0, 18);
            this.GuessedWordsLabel.TabIndex = 4;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorLabel.ForeColor = System.Drawing.Color.Orange;
            this.ErrorLabel.Location = new System.Drawing.Point(781, 513);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 20);
            this.ErrorLabel.TabIndex = 5;
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.BackColor = System.Drawing.Color.Transparent;
            this.ResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResultLabel.ForeColor = System.Drawing.Color.Orange;
            this.ResultLabel.Location = new System.Drawing.Point(968, 395);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(0, 20);
            this.ResultLabel.TabIndex = 6;
            // 
            // ShowPicture1
            // 
            this.ShowPicture1.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture1.Location = new System.Drawing.Point(24, 12);
            this.ShowPicture1.Name = "ShowPicture1";
            this.ShowPicture1.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture1.TabIndex = 7;
            this.ShowPicture1.TabStop = false;
            // 
            // ShowPicture4
            // 
            this.ShowPicture4.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture4.Location = new System.Drawing.Point(24, 304);
            this.ShowPicture4.Name = "ShowPicture4";
            this.ShowPicture4.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture4.TabIndex = 8;
            this.ShowPicture4.TabStop = false;
            // 
            // ShowPicture15
            // 
            this.ShowPicture15.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture15.Location = new System.Drawing.Point(836, 12);
            this.ShowPicture15.Name = "ShowPicture15";
            this.ShowPicture15.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture15.TabIndex = 9;
            this.ShowPicture15.TabStop = false;
            // 
            // ShowPicture9
            // 
            this.ShowPicture9.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture9.Location = new System.Drawing.Point(306, 12);
            this.ShowPicture9.Name = "ShowPicture9";
            this.ShowPicture9.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture9.TabIndex = 10;
            this.ShowPicture9.TabStop = false;
            // 
            // ShowPicture10
            // 
            this.ShowPicture10.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture10.Location = new System.Drawing.Point(12, 447);
            this.ShowPicture10.Name = "ShowPicture10";
            this.ShowPicture10.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture10.TabIndex = 11;
            this.ShowPicture10.TabStop = false;
            // 
            // ShowPicture18
            // 
            this.ShowPicture18.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture18.Location = new System.Drawing.Point(671, 37);
            this.ShowPicture18.Name = "ShowPicture18";
            this.ShowPicture18.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture18.TabIndex = 12;
            this.ShowPicture18.TabStop = false;
            // 
            // ShowPicture16
            // 
            this.ShowPicture16.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture16.Location = new System.Drawing.Point(136, 424);
            this.ShowPicture16.Name = "ShowPicture16";
            this.ShowPicture16.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture16.TabIndex = 14;
            this.ShowPicture16.TabStop = false;
            // 
            // ShowPicture6
            // 
            this.ShowPicture6.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture6.Location = new System.Drawing.Point(153, 256);
            this.ShowPicture6.Name = "ShowPicture6";
            this.ShowPicture6.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture6.TabIndex = 15;
            this.ShowPicture6.TabStop = false;
            // 
            // ShowPicture12
            // 
            this.ShowPicture12.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture12.Location = new System.Drawing.Point(553, 79);
            this.ShowPicture12.Name = "ShowPicture12";
            this.ShowPicture12.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture12.TabIndex = 16;
            this.ShowPicture12.TabStop = false;
            // 
            // ShowPicture2
            // 
            this.ShowPicture2.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture2.Location = new System.Drawing.Point(474, 274);
            this.ShowPicture2.Name = "ShowPicture2";
            this.ShowPicture2.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture2.TabIndex = 17;
            this.ShowPicture2.TabStop = false;
            // 
            // ShowPicture5
            // 
            this.ShowPicture5.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture5.Location = new System.Drawing.Point(598, 247);
            this.ShowPicture5.Name = "ShowPicture5";
            this.ShowPicture5.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture5.TabIndex = 18;
            this.ShowPicture5.TabStop = false;
            // 
            // ShowPicture13
            // 
            this.ShowPicture13.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture13.Location = new System.Drawing.Point(572, 433);
            this.ShowPicture13.Name = "ShowPicture13";
            this.ShowPicture13.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture13.TabIndex = 19;
            this.ShowPicture13.TabStop = false;
            // 
            // ShowPicture7
            // 
            this.ShowPicture7.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture7.Location = new System.Drawing.Point(411, 411);
            this.ShowPicture7.Name = "ShowPicture7";
            this.ShowPicture7.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture7.TabIndex = 20;
            this.ShowPicture7.TabStop = false;
            // 
            // ShowPicture14
            // 
            this.ShowPicture14.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture14.Location = new System.Drawing.Point(317, 256);
            this.ShowPicture14.Name = "ShowPicture14";
            this.ShowPicture14.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture14.TabIndex = 21;
            this.ShowPicture14.TabStop = false;
            // 
            // ShowPicture8
            // 
            this.ShowPicture8.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture8.Location = new System.Drawing.Point(758, 333);
            this.ShowPicture8.Name = "ShowPicture8";
            this.ShowPicture8.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture8.TabIndex = 22;
            this.ShowPicture8.TabStop = false;
            // 
            // ShowPicture19
            // 
            this.ShowPicture19.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture19.Location = new System.Drawing.Point(426, 144);
            this.ShowPicture19.Name = "ShowPicture19";
            this.ShowPicture19.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture19.TabIndex = 23;
            this.ShowPicture19.TabStop = false;
            // 
            // ShowPicture20
            // 
            this.ShowPicture20.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture20.Location = new System.Drawing.Point(199, 115);
            this.ShowPicture20.Name = "ShowPicture20";
            this.ShowPicture20.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture20.TabIndex = 24;
            this.ShowPicture20.TabStop = false;
            // 
            // ShowPicture17
            // 
            this.ShowPicture17.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture17.Location = new System.Drawing.Point(12, 152);
            this.ShowPicture17.Name = "ShowPicture17";
            this.ShowPicture17.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture17.TabIndex = 25;
            this.ShowPicture17.TabStop = false;
            // 
            // ShowPicture3
            // 
            this.ShowPicture3.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture3.Location = new System.Drawing.Point(714, 190);
            this.ShowPicture3.Name = "ShowPicture3";
            this.ShowPicture3.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture3.TabIndex = 26;
            this.ShowPicture3.TabStop = false;
            // 
            // ShowPicture11
            // 
            this.ShowPicture11.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture11.Location = new System.Drawing.Point(279, 433);
            this.ShowPicture11.Name = "ShowPicture11";
            this.ShowPicture11.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture11.TabIndex = 27;
            this.ShowPicture11.TabStop = false;
            // 
            // TextLbl1
            // 
            this.TextLbl1.AutoSize = true;
            this.TextLbl1.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl1.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl1.Location = new System.Drawing.Point(19, 115);
            this.TextLbl1.Name = "TextLbl1";
            this.TextLbl1.Size = new System.Drawing.Size(0, 26);
            this.TextLbl1.TabIndex = 28;
            // 
            // TextLbl15
            // 
            this.TextLbl15.AutoSize = true;
            this.TextLbl15.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl15.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl15.Location = new System.Drawing.Point(831, 115);
            this.TextLbl15.Name = "TextLbl15";
            this.TextLbl15.Size = new System.Drawing.Size(0, 26);
            this.TextLbl15.TabIndex = 29;
            // 
            // TextLbl18
            // 
            this.TextLbl18.AutoSize = true;
            this.TextLbl18.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl18.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl18.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl18.Location = new System.Drawing.Point(666, 140);
            this.TextLbl18.Name = "TextLbl18";
            this.TextLbl18.Size = new System.Drawing.Size(0, 26);
            this.TextLbl18.TabIndex = 30;
            // 
            // TextLbl12
            // 
            this.TextLbl12.AutoSize = true;
            this.TextLbl12.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl12.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl12.Location = new System.Drawing.Point(548, 182);
            this.TextLbl12.Name = "TextLbl12";
            this.TextLbl12.Size = new System.Drawing.Size(0, 26);
            this.TextLbl12.TabIndex = 31;
            // 
            // TextLbl9
            // 
            this.TextLbl9.AutoSize = true;
            this.TextLbl9.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl9.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl9.Location = new System.Drawing.Point(301, 115);
            this.TextLbl9.Name = "TextLbl9";
            this.TextLbl9.Size = new System.Drawing.Size(0, 26);
            this.TextLbl9.TabIndex = 32;
            // 
            // TextLbl20
            // 
            this.TextLbl20.AutoSize = true;
            this.TextLbl20.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl20.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl20.Location = new System.Drawing.Point(194, 218);
            this.TextLbl20.Name = "TextLbl20";
            this.TextLbl20.Size = new System.Drawing.Size(0, 26);
            this.TextLbl20.TabIndex = 33;
            // 
            // TextLbl17
            // 
            this.TextLbl17.AutoSize = true;
            this.TextLbl17.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl17.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl17.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl17.Location = new System.Drawing.Point(7, 256);
            this.TextLbl17.Name = "TextLbl17";
            this.TextLbl17.Size = new System.Drawing.Size(0, 26);
            this.TextLbl17.TabIndex = 34;
            // 
            // TextLbl7
            // 
            this.TextLbl7.AutoSize = true;
            this.TextLbl7.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl7.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl7.Location = new System.Drawing.Point(406, 514);
            this.TextLbl7.Name = "TextLbl7";
            this.TextLbl7.Size = new System.Drawing.Size(0, 26);
            this.TextLbl7.TabIndex = 35;
            // 
            // TextLbl2
            // 
            this.TextLbl2.AutoSize = true;
            this.TextLbl2.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl2.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl2.Location = new System.Drawing.Point(469, 377);
            this.TextLbl2.Name = "TextLbl2";
            this.TextLbl2.Size = new System.Drawing.Size(0, 26);
            this.TextLbl2.TabIndex = 36;
            // 
            // TextLbl14
            // 
            this.TextLbl14.AutoSize = true;
            this.TextLbl14.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl14.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl14.Location = new System.Drawing.Point(312, 359);
            this.TextLbl14.Name = "TextLbl14";
            this.TextLbl14.Size = new System.Drawing.Size(0, 26);
            this.TextLbl14.TabIndex = 37;
            // 
            // TextLbl6
            // 
            this.TextLbl6.AutoSize = true;
            this.TextLbl6.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl6.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl6.Location = new System.Drawing.Point(148, 359);
            this.TextLbl6.Name = "TextLbl6";
            this.TextLbl6.Size = new System.Drawing.Size(0, 26);
            this.TextLbl6.TabIndex = 38;
            // 
            // TextLbl4
            // 
            this.TextLbl4.AutoSize = true;
            this.TextLbl4.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl4.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl4.Location = new System.Drawing.Point(19, 407);
            this.TextLbl4.Name = "TextLbl4";
            this.TextLbl4.Size = new System.Drawing.Size(0, 26);
            this.TextLbl4.TabIndex = 39;
            // 
            // TextLbl19
            // 
            this.TextLbl19.AutoSize = true;
            this.TextLbl19.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl19.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl19.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl19.Location = new System.Drawing.Point(421, 245);
            this.TextLbl19.Name = "TextLbl19";
            this.TextLbl19.Size = new System.Drawing.Size(0, 26);
            this.TextLbl19.TabIndex = 40;
            // 
            // TextLbl5
            // 
            this.TextLbl5.AutoSize = true;
            this.TextLbl5.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl5.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl5.Location = new System.Drawing.Point(593, 350);
            this.TextLbl5.Name = "TextLbl5";
            this.TextLbl5.Size = new System.Drawing.Size(0, 26);
            this.TextLbl5.TabIndex = 41;
            // 
            // TextLbl13
            // 
            this.TextLbl13.AutoSize = true;
            this.TextLbl13.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl13.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl13.Location = new System.Drawing.Point(567, 536);
            this.TextLbl13.Name = "TextLbl13";
            this.TextLbl13.Size = new System.Drawing.Size(0, 26);
            this.TextLbl13.TabIndex = 42;
            // 
            // TextLbl11
            // 
            this.TextLbl11.AutoSize = true;
            this.TextLbl11.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl11.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl11.Location = new System.Drawing.Point(274, 536);
            this.TextLbl11.Name = "TextLbl11";
            this.TextLbl11.Size = new System.Drawing.Size(0, 26);
            this.TextLbl11.TabIndex = 43;
            // 
            // TextLbl10
            // 
            this.TextLbl10.AutoSize = true;
            this.TextLbl10.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl10.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl10.Location = new System.Drawing.Point(7, 550);
            this.TextLbl10.Name = "TextLbl10";
            this.TextLbl10.Size = new System.Drawing.Size(0, 26);
            this.TextLbl10.TabIndex = 44;
            // 
            // TextLbl16
            // 
            this.TextLbl16.AutoSize = true;
            this.TextLbl16.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl16.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl16.Location = new System.Drawing.Point(131, 527);
            this.TextLbl16.Name = "TextLbl16";
            this.TextLbl16.Size = new System.Drawing.Size(0, 26);
            this.TextLbl16.TabIndex = 45;
            // 
            // TextLbl8
            // 
            this.TextLbl8.AutoSize = true;
            this.TextLbl8.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl8.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl8.Location = new System.Drawing.Point(753, 436);
            this.TextLbl8.Name = "TextLbl8";
            this.TextLbl8.Size = new System.Drawing.Size(0, 26);
            this.TextLbl8.TabIndex = 47;
            // 
            // TextLbl3
            // 
            this.TextLbl3.AutoSize = true;
            this.TextLbl3.BackColor = System.Drawing.Color.Transparent;
            this.TextLbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLbl3.ForeColor = System.Drawing.Color.Orange;
            this.TextLbl3.Location = new System.Drawing.Point(709, 293);
            this.TextLbl3.Name = "TextLbl3";
            this.TextLbl3.Size = new System.Drawing.Size(0, 26);
            this.TextLbl3.TabIndex = 48;
            // 
            // Exercise2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1144, 579);
            this.Controls.Add(this.TextLbl3);
            this.Controls.Add(this.TextLbl8);
            this.Controls.Add(this.TextLbl16);
            this.Controls.Add(this.TextLbl10);
            this.Controls.Add(this.TextLbl11);
            this.Controls.Add(this.TextLbl13);
            this.Controls.Add(this.TextLbl5);
            this.Controls.Add(this.TextLbl19);
            this.Controls.Add(this.TextLbl4);
            this.Controls.Add(this.TextLbl6);
            this.Controls.Add(this.TextLbl14);
            this.Controls.Add(this.TextLbl2);
            this.Controls.Add(this.TextLbl7);
            this.Controls.Add(this.TextLbl17);
            this.Controls.Add(this.TextLbl20);
            this.Controls.Add(this.TextLbl9);
            this.Controls.Add(this.TextLbl12);
            this.Controls.Add(this.TextLbl18);
            this.Controls.Add(this.TextLbl15);
            this.Controls.Add(this.TextLbl1);
            this.Controls.Add(this.ShowPicture11);
            this.Controls.Add(this.ShowPicture3);
            this.Controls.Add(this.ShowPicture17);
            this.Controls.Add(this.ShowPicture20);
            this.Controls.Add(this.ShowPicture19);
            this.Controls.Add(this.ShowPicture8);
            this.Controls.Add(this.ShowPicture14);
            this.Controls.Add(this.ShowPicture7);
            this.Controls.Add(this.ShowPicture13);
            this.Controls.Add(this.ShowPicture5);
            this.Controls.Add(this.ShowPicture2);
            this.Controls.Add(this.ShowPicture12);
            this.Controls.Add(this.ShowPicture6);
            this.Controls.Add(this.ShowPicture16);
            this.Controls.Add(this.ShowPicture18);
            this.Controls.Add(this.ShowPicture10);
            this.Controls.Add(this.ShowPicture9);
            this.Controls.Add(this.ShowPicture15);
            this.Controls.Add(this.ShowPicture4);
            this.Controls.Add(this.ShowPicture1);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.GuessedWordsLabel);
            this.Controls.Add(this.InputWordTextBox);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Name = "Exercise2Form";
            this.Text = "Exercice 2 - Mémoire visuelle / vision globale";
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.TextBox InputWordTextBox;
        private System.Windows.Forms.Label GuessedWordsLabel;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.PictureBox ShowPicture1;
        private System.Windows.Forms.PictureBox ShowPicture4;
        private System.Windows.Forms.PictureBox ShowPicture15;
        private System.Windows.Forms.PictureBox ShowPicture9;
        private System.Windows.Forms.PictureBox ShowPicture10;
        private System.Windows.Forms.PictureBox ShowPicture18;
        private System.Windows.Forms.PictureBox ShowPicture16;
        private System.Windows.Forms.PictureBox ShowPicture6;
        private System.Windows.Forms.PictureBox ShowPicture12;
        private System.Windows.Forms.PictureBox ShowPicture2;
        private System.Windows.Forms.PictureBox ShowPicture5;
        private System.Windows.Forms.PictureBox ShowPicture13;
        private System.Windows.Forms.PictureBox ShowPicture7;
        private System.Windows.Forms.PictureBox ShowPicture14;
        private System.Windows.Forms.PictureBox ShowPicture8;
        private System.Windows.Forms.PictureBox ShowPicture19;
        private System.Windows.Forms.PictureBox ShowPicture20;
        private System.Windows.Forms.PictureBox ShowPicture17;
        private System.Windows.Forms.PictureBox ShowPicture3;
        private System.Windows.Forms.PictureBox ShowPicture11;
        private System.Windows.Forms.Label TextLbl1;
        private System.Windows.Forms.Label TextLbl15;
        private System.Windows.Forms.Label TextLbl18;
        private System.Windows.Forms.Label TextLbl12;
        private System.Windows.Forms.Label TextLbl9;
        private System.Windows.Forms.Label TextLbl20;
        private System.Windows.Forms.Label TextLbl17;
        private System.Windows.Forms.Label TextLbl7;
        private System.Windows.Forms.Label TextLbl2;
        private System.Windows.Forms.Label TextLbl14;
        private System.Windows.Forms.Label TextLbl6;
        private System.Windows.Forms.Label TextLbl4;
        private System.Windows.Forms.Label TextLbl19;
        private System.Windows.Forms.Label TextLbl5;
        private System.Windows.Forms.Label TextLbl13;
        private System.Windows.Forms.Label TextLbl11;
        private System.Windows.Forms.Label TextLbl10;
        private System.Windows.Forms.Label TextLbl16;
        private System.Windows.Forms.Label TextLbl8;
        private System.Windows.Forms.Label TextLbl3;
    }
}