﻿using Memorazing.Models;
using Memorazing.soundex;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.Exercises
{
    public partial class Exercise2Form : Form
    {
        private List<PictureBox> pictures;
        private List<Label> labels;
        private int guessesMade = 0;
        ProcessWord processWord;

        public Exercise2Form()
        {
            InitializeComponent();
            pictures =new List<PictureBox>()
            {
                ShowPicture1,ShowPicture2,ShowPicture3,ShowPicture4,ShowPicture5,
                ShowPicture6,ShowPicture7,ShowPicture8,ShowPicture9,ShowPicture10,
                ShowPicture11,ShowPicture12,ShowPicture13,ShowPicture14,ShowPicture15,
                ShowPicture16,ShowPicture17,ShowPicture18,ShowPicture19,ShowPicture20
            };
            labels = new List<Label>()
            {
                TextLbl1,TextLbl2,TextLbl3,TextLbl4,TextLbl5,TextLbl6,TextLbl7,TextLbl8,TextLbl9,TextLbl10,
                TextLbl11,TextLbl12,TextLbl13,TextLbl14,TextLbl5,TextLbl6,TextLbl17,TextLbl18,TextLbl19,TextLbl20
            };
        }

        public List<PictureBox> Pictures
        {
            get{ return pictures; }
        }

        public List<Label> Labels
        {
            get { return labels; }
        }

        public void InitializePictureImageAndWord(int pictureCount,Bitmap img, string title)
        {
            Pictures[pictureCount].Image = img;
            labels[pictureCount].Text = title;
        }

        public void ProcessWordInitialize(List<Word> words)
        {
            processWord = new ProcessWord(words);
        }

        public void AfterExercise()
        {
            for (int i = 0; i < Pictures.Count; i++)
            {
                Pictures[i].Image = null;
                Labels[i].Text = null;
            }
            SubmitWordBtn.Visible = true;
            InputWordTextBox.Visible = true;
        }
        
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SubmitWordBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string guessedWord = InputWordTextBox.Text.TrimStart().TrimEnd().ToLower();
                string output = guessedWord;
                bool wordIsInList = processWord.GuessedWordInList(guessedWord);
                if (wordIsInList)
                {
                    output += "----O";
                }
                else
                {
                    output += "----X";
                }

                GuessedWordsLabel.Text += output + " \r\n";
                guessesMade++;
                if (guessesMade >= Options.NrOfWords) endForm();

            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
        }

        private void endForm()
        {
            InputWordTextBox.Enabled = false;
            SubmitWordBtn.Enabled = false;
            ResultLabel.Text = string.Format("You have guessed {0} out of {1} words", processWord.guessedWords, Options.NrOfWords);
            History.addHistoryItem(new HistoryItem(processWord.guessedWords, Options.NrOfWords, 2));
        }
    }
}
