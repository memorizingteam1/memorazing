﻿namespace Memorazing.Exercises
{
    partial class Exercise1Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exercise1Form));
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.guessedWordsLabel = new System.Windows.Forms.Label();
            this.ShowWordLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Crimson;
            this.ExitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.White;
            this.ExitBtn.Location = new System.Drawing.Point(658, 379);
            this.ExitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(107, 40);
            this.ExitBtn.TabIndex = 6;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.BackColor = System.Drawing.Color.Crimson;
            this.SubmitWordBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitWordBtn.ForeColor = System.Drawing.Color.White;
            this.SubmitWordBtn.Location = new System.Drawing.Point(495, 297);
            this.SubmitWordBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(107, 40);
            this.SubmitWordBtn.TabIndex = 5;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = false;
            this.SubmitWordBtn.Visible = false;
            this.SubmitWordBtn.Click += new System.EventHandler(this.SubmitWordBtn_Click);
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(327, 309);
            this.InputWordTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(136, 20);
            this.InputWordTextBox.TabIndex = 4;
            this.InputWordTextBox.Visible = false;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorLabel.ForeColor = System.Drawing.Color.Crimson;
            this.ErrorLabel.Location = new System.Drawing.Point(323, 345);
            this.ErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 20);
            this.ErrorLabel.TabIndex = 8;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.BackColor = System.Drawing.Color.Transparent;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.Crimson;
            this.resultLabel.Location = new System.Drawing.Point(491, 218);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 20);
            this.resultLabel.TabIndex = 7;
            // 
            // guessedWordsLabel
            // 
            this.guessedWordsLabel.AutoSize = true;
            this.guessedWordsLabel.BackColor = System.Drawing.Color.Transparent;
            this.guessedWordsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guessedWordsLabel.ForeColor = System.Drawing.Color.Crimson;
            this.guessedWordsLabel.Location = new System.Drawing.Point(602, 42);
            this.guessedWordsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.guessedWordsLabel.Name = "guessedWordsLabel";
            this.guessedWordsLabel.Size = new System.Drawing.Size(0, 20);
            this.guessedWordsLabel.TabIndex = 9;
            // 
            // ShowWordLbl
            // 
            this.ShowWordLbl.AutoSize = true;
            this.ShowWordLbl.BackColor = System.Drawing.Color.Transparent;
            this.ShowWordLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowWordLbl.ForeColor = System.Drawing.Color.Crimson;
            this.ShowWordLbl.Location = new System.Drawing.Point(46, 42);
            this.ShowWordLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ShowWordLbl.Name = "ShowWordLbl";
            this.ShowWordLbl.Size = new System.Drawing.Size(0, 26);
            this.ShowWordLbl.TabIndex = 10;
            // 
            // Exercise1Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(776, 453);
            this.Controls.Add(this.ShowWordLbl);
            this.Controls.Add(this.guessedWordsLabel);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Controls.Add(this.InputWordTextBox);
            this.Name = "Exercise1Form";
            this.Text = "Exercice 1 - Mémoire visuelle / linéaire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.TextBox InputWordTextBox;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label guessedWordsLabel;
        private System.Windows.Forms.Label ShowWordLbl;
    }
}