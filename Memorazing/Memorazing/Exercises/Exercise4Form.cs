﻿using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.Exercises
{
    public partial class Exercise4Form : Form
    {
        //TextBox txtBox = null;
        //Button button = null;
        List<TextBox> txtBoxAssociations = new List<TextBox>();
        List<Button> btnValidate = new List<Button>();
        List<Label> lblWordsAssociations = new List<Label>();
        List<Label> lblWordsBase = new List<Label>();


        List<Word> words = new List<Word>();
        ProcessWord processWord;

        private int guessesMade = 0;
        private int guessedWords = 0;
        private List<String> alreadyGuessedWords = new List<string>();


        public Exercise4Form()
        {
            Random rnd = new Random();
            int month = rnd.Next(0, Main.DictioWords.Count);
            string randomCategorie = Main.DictioWords.ElementAt(month).Key;
            Main.DictioWords.TryGetValue(randomCategorie, out words);
            processWord = new ProcessWord(words);

            InitializeComponent();
            StartShowingAllTheWords();
        }


        private void StartShowingAllTheWords()
        {
            int nrOfWord = 0;
            this.Update();
            foreach (Word w in words)
            {
                if (nrOfWord < Options.NrOfWords)
                {
                    //await Task.Delay(Options.WordSpeed * 1000);
                    lblWordsBase.Add(generateLabel(75 + 25*nrOfWord, nrOfWord, w.Title));
                    //ShowWordLbl.Text += w.Title + " \r\n";
                    txtBoxAssociations.Add(generateTxtBox(75 + 25*nrOfWord, nrOfWord));
                    
                    //w.WordAssociation = txtBox.Text;
                    //lbl_WordAssociation.Text += w.WordAssociation + " \r\n";
                    btnValidate.Add(generateButton(75 + 25 * nrOfWord, nrOfWord));
                    if(nrOfWord != 0)
                    {
                        lblWordsBase[nrOfWord].Visible = false;
                        txtBoxAssociations[nrOfWord].Visible = false;
                        btnValidate[nrOfWord].Visible = false;
                        //lblWords[nrOfWord].Visible = false;
                    }
                    nrOfWord++;
                    this.Update();
                }
            }
            //await Task.Delay(Options.WordSpeed * 1000);

            /*lbl_WordAssociation.Text = "";
            ShowWordLbl.Text = "";
            SubmitWordBtn.Visible = false;
            InputWordTextBox.Visible = false;*/

            //txtBox.Visible = false;
        }

        private TextBox generateTxtBox(int locationTxtBox, int nrTxtBox)
        {
            TextBox tb = new TextBox();
            tb.Location = new Point(200, locationTxtBox);
            tb.Width = 100;
            tb.Name = "" + nrTxtBox;
            this.Controls.Add(tb);
            return tb;
        }

        private Button generateButton(int locationButton, int nrButton)
        {
            Button bouton = new Button();
            bouton.Text = "Valider";
            bouton.Location = new Point(400, locationButton);
            bouton.Size = new Size(100, 28);
            //bouton.BackColor = Color.Cyan;
            bouton.Name = "" + nrButton;
            bouton.Click += Bouton_Click;
            this.Controls.Add(bouton);
            return bouton;
        }

        private void Bouton_Click(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            int number = 0;
            int.TryParse(bouton.Name, out number);
            words[number].WordAssociation = txtBoxAssociations[number].Text;
            lbl_WordAssociation.Text += txtBoxAssociations[number].Text + " \r\n";
            lbl_WordAssociation.Visible = true;
            bouton.Visible = false;
            txtBoxAssociations[number].Visible = false;
            if (number < words.Count - 1)
            {
                txtBoxAssociations[number+1].Visible = true;
                btnValidate[number+1].Visible = true;
                lblWordsBase[number+1].Visible = true;
            }
            else
            {
                //lbl_WordAssociation.Text = "";
                //ShowWordLbl.Text = "";
                for(int i = 0; i <= number; i++)
                {
                    lblWordsBase[i].Visible = false;
                }
                SubmitWordBtn.Visible = true;
                InputWordTextBox.Visible = true;
                ListForms.ExportListToFile();
            }
        }

         private Label generateLabel(int locationLabel, int nrLabel, string text)
         {
            Label lb = new Label();
            lb.Location = new Point(0, locationLabel);
            lb.Text = text;
            lb.BackColor = Color.Transparent;
            lb.ForeColor = Color.ForestGreen;
            lb.Font = new Font(Font.FontFamily, 16);
            lb.Size = new Size(200, 30);

            this.Controls.Add(lb);
            return lb;
         }
    }
}
