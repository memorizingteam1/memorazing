﻿namespace Memorazing.Exercises
{
    partial class Exercise4Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exercise4Form));
            this.ShowWordLbl = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.lbl_WordAssociation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ShowWordLbl
            // 
            this.ShowWordLbl.AutoSize = true;
            this.ShowWordLbl.BackColor = System.Drawing.Color.Transparent;
            this.ShowWordLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowWordLbl.Location = new System.Drawing.Point(56, 65);
            this.ShowWordLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ShowWordLbl.Name = "ShowWordLbl";
            this.ShowWordLbl.Size = new System.Drawing.Size(0, 26);
            this.ShowWordLbl.TabIndex = 11;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Location = new System.Drawing.Point(166, 311);
            this.ErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorLabel.TabIndex = 15;
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(482, 311);
            this.ExitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(56, 19);
            this.ExitBtn.TabIndex = 14;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.Location = new System.Drawing.Point(317, 262);
            this.SubmitWordBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(75, 23);
            this.SubmitWordBtn.TabIndex = 13;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = true;
            this.SubmitWordBtn.Visible = false;
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(169, 263);
            this.InputWordTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(136, 20);
            this.InputWordTextBox.TabIndex = 12;
            this.InputWordTextBox.Visible = false;
            // 
            // lbl_WordAssociation
            // 
            this.lbl_WordAssociation.AutoSize = true;
            this.lbl_WordAssociation.BackColor = System.Drawing.Color.Transparent;
            this.lbl_WordAssociation.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_WordAssociation.Location = new System.Drawing.Point(202, 65);
            this.lbl_WordAssociation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_WordAssociation.Name = "lbl_WordAssociation";
            this.lbl_WordAssociation.Size = new System.Drawing.Size(0, 26);
            this.lbl_WordAssociation.TabIndex = 16;
            // 
            // Exercise4Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.lbl_WordAssociation);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Controls.Add(this.InputWordTextBox);
            this.Controls.Add(this.ShowWordLbl);
            this.Name = "Exercise4Form";
            this.Text = "Exercice 4 - Mémoire associative / vision globale";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ShowWordLbl;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.TextBox InputWordTextBox;
        private System.Windows.Forms.Label lbl_WordAssociation;
    }
}