﻿namespace Memorazing.Exercises
{
    partial class Exercise3Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exercise3Form));
            this.guessedWordsLabel = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.ShowPicture1 = new System.Windows.Forms.PictureBox();
            this.ShowPicture2 = new System.Windows.Forms.PictureBox();
            this.ShowPicture10 = new System.Windows.Forms.PictureBox();
            this.ShowPicture9 = new System.Windows.Forms.PictureBox();
            this.ShowPicture8 = new System.Windows.Forms.PictureBox();
            this.ShowPicture7 = new System.Windows.Forms.PictureBox();
            this.ShowPicture6 = new System.Windows.Forms.PictureBox();
            this.ShowPicture5 = new System.Windows.Forms.PictureBox();
            this.ShowPicture4 = new System.Windows.Forms.PictureBox();
            this.ShowPicture3 = new System.Windows.Forms.PictureBox();
            this.ShowPicture11 = new System.Windows.Forms.PictureBox();
            this.ShowPicture12 = new System.Windows.Forms.PictureBox();
            this.ShowPicture13 = new System.Windows.Forms.PictureBox();
            this.ShowPicture15 = new System.Windows.Forms.PictureBox();
            this.ShowPicture19 = new System.Windows.Forms.PictureBox();
            this.ShowPicture20 = new System.Windows.Forms.PictureBox();
            this.ShowPicture16 = new System.Windows.Forms.PictureBox();
            this.ShowPicture17 = new System.Windows.Forms.PictureBox();
            this.ShowPicture14 = new System.Windows.Forms.PictureBox();
            this.ShowPicture18 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture18)).BeginInit();
            this.SuspendLayout();
            // 
            // guessedWordsLabel
            // 
            this.guessedWordsLabel.AutoSize = true;
            this.guessedWordsLabel.BackColor = System.Drawing.Color.Transparent;
            this.guessedWordsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guessedWordsLabel.ForeColor = System.Drawing.Color.Goldenrod;
            this.guessedWordsLabel.Location = new System.Drawing.Point(749, 20);
            this.guessedWordsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.guessedWordsLabel.Name = "guessedWordsLabel";
            this.guessedWordsLabel.Size = new System.Drawing.Size(0, 20);
            this.guessedWordsLabel.TabIndex = 16;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorLabel.ForeColor = System.Drawing.Color.Goldenrod;
            this.ErrorLabel.Location = new System.Drawing.Point(740, 380);
            this.ErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 20);
            this.ErrorLabel.TabIndex = 15;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.BackColor = System.Drawing.Color.Transparent;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.Goldenrod;
            this.resultLabel.Location = new System.Drawing.Point(749, 298);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 20);
            this.resultLabel.TabIndex = 14;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Goldenrod;
            this.ExitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.White;
            this.ExitBtn.Location = new System.Drawing.Point(928, 440);
            this.ExitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(107, 40);
            this.ExitBtn.TabIndex = 13;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.BackColor = System.Drawing.Color.Goldenrod;
            this.SubmitWordBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitWordBtn.ForeColor = System.Drawing.Color.White;
            this.SubmitWordBtn.Location = new System.Drawing.Point(905, 328);
            this.SubmitWordBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(107, 40);
            this.SubmitWordBtn.TabIndex = 12;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = false;
            this.SubmitWordBtn.Visible = false;
            this.SubmitWordBtn.Click += new System.EventHandler(this.SubmitWordBtn_Click);
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(753, 340);
            this.InputWordTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(136, 20);
            this.InputWordTextBox.TabIndex = 11;
            this.InputWordTextBox.Visible = false;
            // 
            // ShowPicture1
            // 
            this.ShowPicture1.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture1.Location = new System.Drawing.Point(20, 20);
            this.ShowPicture1.Name = "ShowPicture1";
            this.ShowPicture1.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture1.TabIndex = 18;
            this.ShowPicture1.TabStop = false;
            // 
            // ShowPicture2
            // 
            this.ShowPicture2.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture2.Location = new System.Drawing.Point(170, 20);
            this.ShowPicture2.Name = "ShowPicture2";
            this.ShowPicture2.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture2.TabIndex = 19;
            this.ShowPicture2.TabStop = false;
            // 
            // ShowPicture10
            // 
            this.ShowPicture10.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture10.Location = new System.Drawing.Point(620, 140);
            this.ShowPicture10.Name = "ShowPicture10";
            this.ShowPicture10.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture10.TabIndex = 20;
            this.ShowPicture10.TabStop = false;
            // 
            // ShowPicture9
            // 
            this.ShowPicture9.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture9.Location = new System.Drawing.Point(470, 140);
            this.ShowPicture9.Name = "ShowPicture9";
            this.ShowPicture9.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture9.TabIndex = 21;
            this.ShowPicture9.TabStop = false;
            // 
            // ShowPicture8
            // 
            this.ShowPicture8.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture8.Location = new System.Drawing.Point(320, 140);
            this.ShowPicture8.Name = "ShowPicture8";
            this.ShowPicture8.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture8.TabIndex = 22;
            this.ShowPicture8.TabStop = false;
            // 
            // ShowPicture7
            // 
            this.ShowPicture7.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture7.Location = new System.Drawing.Point(170, 140);
            this.ShowPicture7.Name = "ShowPicture7";
            this.ShowPicture7.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture7.TabIndex = 23;
            this.ShowPicture7.TabStop = false;
            // 
            // ShowPicture6
            // 
            this.ShowPicture6.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture6.Location = new System.Drawing.Point(20, 140);
            this.ShowPicture6.Name = "ShowPicture6";
            this.ShowPicture6.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture6.TabIndex = 24;
            this.ShowPicture6.TabStop = false;
            // 
            // ShowPicture5
            // 
            this.ShowPicture5.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture5.Location = new System.Drawing.Point(620, 20);
            this.ShowPicture5.Name = "ShowPicture5";
            this.ShowPicture5.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture5.TabIndex = 25;
            this.ShowPicture5.TabStop = false;
            // 
            // ShowPicture4
            // 
            this.ShowPicture4.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture4.Location = new System.Drawing.Point(470, 20);
            this.ShowPicture4.Name = "ShowPicture4";
            this.ShowPicture4.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture4.TabIndex = 26;
            this.ShowPicture4.TabStop = false;
            // 
            // ShowPicture3
            // 
            this.ShowPicture3.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture3.Location = new System.Drawing.Point(320, 20);
            this.ShowPicture3.Name = "ShowPicture3";
            this.ShowPicture3.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture3.TabIndex = 27;
            this.ShowPicture3.TabStop = false;
            // 
            // ShowPicture11
            // 
            this.ShowPicture11.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture11.Location = new System.Drawing.Point(20, 260);
            this.ShowPicture11.Name = "ShowPicture11";
            this.ShowPicture11.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture11.TabIndex = 32;
            this.ShowPicture11.TabStop = false;
            // 
            // ShowPicture12
            // 
            this.ShowPicture12.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture12.Location = new System.Drawing.Point(170, 260);
            this.ShowPicture12.Name = "ShowPicture12";
            this.ShowPicture12.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture12.TabIndex = 31;
            this.ShowPicture12.TabStop = false;
            // 
            // ShowPicture13
            // 
            this.ShowPicture13.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture13.Location = new System.Drawing.Point(320, 260);
            this.ShowPicture13.Name = "ShowPicture13";
            this.ShowPicture13.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture13.TabIndex = 30;
            this.ShowPicture13.TabStop = false;
            // 
            // ShowPicture15
            // 
            this.ShowPicture15.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture15.Location = new System.Drawing.Point(620, 260);
            this.ShowPicture15.Name = "ShowPicture15";
            this.ShowPicture15.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture15.TabIndex = 29;
            this.ShowPicture15.TabStop = false;
            // 
            // ShowPicture19
            // 
            this.ShowPicture19.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture19.Location = new System.Drawing.Point(473, 380);
            this.ShowPicture19.Name = "ShowPicture19";
            this.ShowPicture19.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture19.TabIndex = 28;
            this.ShowPicture19.TabStop = false;
            // 
            // ShowPicture20
            // 
            this.ShowPicture20.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture20.Location = new System.Drawing.Point(620, 380);
            this.ShowPicture20.Name = "ShowPicture20";
            this.ShowPicture20.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture20.TabIndex = 33;
            this.ShowPicture20.TabStop = false;
            // 
            // ShowPicture16
            // 
            this.ShowPicture16.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture16.Location = new System.Drawing.Point(20, 380);
            this.ShowPicture16.Name = "ShowPicture16";
            this.ShowPicture16.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture16.TabIndex = 34;
            this.ShowPicture16.TabStop = false;
            // 
            // ShowPicture17
            // 
            this.ShowPicture17.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture17.Location = new System.Drawing.Point(170, 380);
            this.ShowPicture17.Name = "ShowPicture17";
            this.ShowPicture17.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture17.TabIndex = 35;
            this.ShowPicture17.TabStop = false;
            // 
            // ShowPicture14
            // 
            this.ShowPicture14.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture14.Location = new System.Drawing.Point(473, 260);
            this.ShowPicture14.Name = "ShowPicture14";
            this.ShowPicture14.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture14.TabIndex = 36;
            this.ShowPicture14.TabStop = false;
            // 
            // ShowPicture18
            // 
            this.ShowPicture18.BackColor = System.Drawing.Color.Transparent;
            this.ShowPicture18.Location = new System.Drawing.Point(320, 380);
            this.ShowPicture18.Name = "ShowPicture18";
            this.ShowPicture18.Size = new System.Drawing.Size(100, 100);
            this.ShowPicture18.TabIndex = 37;
            this.ShowPicture18.TabStop = false;
            // 
            // Exercise3Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1046, 496);
            this.Controls.Add(this.ShowPicture18);
            this.Controls.Add(this.ShowPicture14);
            this.Controls.Add(this.ShowPicture17);
            this.Controls.Add(this.ShowPicture16);
            this.Controls.Add(this.ShowPicture20);
            this.Controls.Add(this.ShowPicture11);
            this.Controls.Add(this.ShowPicture12);
            this.Controls.Add(this.ShowPicture13);
            this.Controls.Add(this.ShowPicture15);
            this.Controls.Add(this.ShowPicture19);
            this.Controls.Add(this.ShowPicture3);
            this.Controls.Add(this.ShowPicture4);
            this.Controls.Add(this.ShowPicture5);
            this.Controls.Add(this.ShowPicture6);
            this.Controls.Add(this.ShowPicture7);
            this.Controls.Add(this.ShowPicture8);
            this.Controls.Add(this.ShowPicture9);
            this.Controls.Add(this.ShowPicture10);
            this.Controls.Add(this.ShowPicture2);
            this.Controls.Add(this.ShowPicture1);
            this.Controls.Add(this.guessedWordsLabel);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Controls.Add(this.InputWordTextBox);
            this.Name = "Exercise3Form";
            this.Text = "Exercice 3 - Mémoire auditive / linéaire";
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPicture18)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label guessedWordsLabel;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.TextBox InputWordTextBox;
        private System.Windows.Forms.PictureBox ShowPicture1;
        private System.Windows.Forms.PictureBox ShowPicture2;
        private System.Windows.Forms.PictureBox ShowPicture10;
        private System.Windows.Forms.PictureBox ShowPicture9;
        private System.Windows.Forms.PictureBox ShowPicture8;
        private System.Windows.Forms.PictureBox ShowPicture7;
        private System.Windows.Forms.PictureBox ShowPicture6;
        private System.Windows.Forms.PictureBox ShowPicture5;
        private System.Windows.Forms.PictureBox ShowPicture4;
        private System.Windows.Forms.PictureBox ShowPicture3;
        private System.Windows.Forms.PictureBox ShowPicture11;
        private System.Windows.Forms.PictureBox ShowPicture12;
        private System.Windows.Forms.PictureBox ShowPicture13;
        private System.Windows.Forms.PictureBox ShowPicture15;
        private System.Windows.Forms.PictureBox ShowPicture19;
        private System.Windows.Forms.PictureBox ShowPicture20;
        private System.Windows.Forms.PictureBox ShowPicture16;
        private System.Windows.Forms.PictureBox ShowPicture17;
        private System.Windows.Forms.PictureBox ShowPicture14;
        private System.Windows.Forms.PictureBox ShowPicture18;
    }
}