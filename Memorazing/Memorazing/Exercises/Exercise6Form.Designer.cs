﻿namespace Memorazing.Exercises
{
    partial class Exercise6Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exercise6Form));
            this.ShowWordLbl = new System.Windows.Forms.Label();
            this.guessedWordsLabel = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ShowWordLbl
            // 
            this.ShowWordLbl.AutoSize = true;
            this.ShowWordLbl.BackColor = System.Drawing.Color.Transparent;
            this.ShowWordLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowWordLbl.Location = new System.Drawing.Point(56, 54);
            this.ShowWordLbl.Name = "ShowWordLbl";
            this.ShowWordLbl.Size = new System.Drawing.Size(0, 31);
            this.ShowWordLbl.TabIndex = 24;
            // 
            // guessedWordsLabel
            // 
            this.guessedWordsLabel.AutoSize = true;
            this.guessedWordsLabel.BackColor = System.Drawing.Color.Transparent;
            this.guessedWordsLabel.Location = new System.Drawing.Point(562, 54);
            this.guessedWordsLabel.Name = "guessedWordsLabel";
            this.guessedWordsLabel.Size = new System.Drawing.Size(0, 17);
            this.guessedWordsLabel.TabIndex = 23;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Location = new System.Drawing.Point(165, 374);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.ErrorLabel.TabIndex = 22;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.BackColor = System.Drawing.Color.Transparent;
            this.resultLabel.Location = new System.Drawing.Point(399, 132);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 17);
            this.resultLabel.TabIndex = 21;
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(631, 363);
            this.ExitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(92, 28);
            this.ExitBtn.TabIndex = 20;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.Location = new System.Drawing.Point(496, 363);
            this.SubmitWordBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(100, 28);
            this.SubmitWordBtn.TabIndex = 19;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = true;
            this.SubmitWordBtn.Visible = false;
            this.SubmitWordBtn.Click += new System.EventHandler(this.SubmitWordBtn_Click);
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(168, 251);
            this.InputWordTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InputWordTextBox.Multiline = true;
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(244, 115);
            this.InputWordTextBox.TabIndex = 18;
            this.InputWordTextBox.Visible = false;
            // 
            // Exercise6Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(779, 444);
            this.Controls.Add(this.ShowWordLbl);
            this.Controls.Add(this.guessedWordsLabel);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Controls.Add(this.InputWordTextBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Exercise6Form";
            this.Text = "Exercice 6 - Créativité et sens";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ShowWordLbl;
        private System.Windows.Forms.Label guessedWordsLabel;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.TextBox InputWordTextBox;
    }
}