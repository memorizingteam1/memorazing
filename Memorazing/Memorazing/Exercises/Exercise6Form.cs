﻿using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Memorazing.soundex;

namespace Memorazing.Exercises
{
    public partial class Exercise6Form : Form
    {
        List<Word> words = new List<Word>();
        private int guessedWords = 0;
        private List<string> alreadyGuessedWords = new List<string>();
        private List<string> alreadyTestedWords = new List<string>();
        private List<Word> shownWords = new List<Word>();
        ProcessWord processWord;
        Random rnd = new Random();

        public Exercise6Form()
        {
            int month = rnd.Next(0, Main.DictioWords.Count);
            string randomCategorie = Main.DictioWords.ElementAt(month).Key;
            Main.DictioWords.TryGetValue(randomCategorie, out words);
            InitializeComponent();
            StartShowingAllTheWords();
            processWord = new ProcessWord(shownWords);
        }

        private async void StartShowingAllTheWords()
        {
            int nrOfWord = 0;
            foreach (Word w in words)
            {
                if (nrOfWord < Options.NrOfWords)
                {
                    shownWords.Add(w);
                    await Task.Delay(Options.WordSpeed * 1000);
                    ShowWordLbl.Text += w.Title + " \r\n";
                    nrOfWord++;
                    this.Update();
                }
            }
            await Task.Delay(Options.WordSpeed * 1000);
            ShowWordLbl.Text = "";         
            SubmitWordBtn.Visible = true;
            InputWordTextBox.Visible = true;
        }


        private void endForm()
        {
            InputWordTextBox.Enabled = false;
            SubmitWordBtn.Enabled = false;
            resultLabel.Text = string.Format("You have guessed {0} out of {1} words", processWord.guessedWords, Options.NrOfWords);
            History.addHistoryItem(new HistoryItem(processWord.guessedWords, Options.NrOfWords, 6));
        }

        private void SubmitWordBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string story = InputWordTextBox.Text.TrimStart().TrimEnd().ToLower();
                string[] words = story.Split(' ');
                foreach (string word in words)
                {
                    if (!alreadyTestedWords.Contains(word))
                    {
                        alreadyTestedWords.Add(word);
                        string output = word;
                        bool wordIsInList = processWord.GuessedWordinListWithoutExceptionForDoubles(word);
                        if (wordIsInList)
                        {
                            output += "----O";
                            guessedWords++;
                            guessedWordsLabel.Text += output + " \r\n";
                        }
                    }
                }
                endForm();
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }

        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
