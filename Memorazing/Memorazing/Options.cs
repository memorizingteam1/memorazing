﻿using System;
namespace Memorazing
{
    [Serializable]
    public class Options
    {
        public static int NrOfWords { get; set; } = 10;
        public static int WordSpeed { get; set; } = 5;

    }
}