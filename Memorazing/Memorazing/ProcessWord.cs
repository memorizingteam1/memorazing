﻿using Memorazing.soundex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memorazing.Models
{
    class ProcessWord
    {
        List<Word> words;
        SoundexEN soundexEN = new SoundexEN();
        private List<string> alreadyGuessedWords = new List<string>();
        public int guessedWords { get; set; }

        public ProcessWord(List<Word> words)
        {
            this.words = words;
        }

        public bool GuessedWordInList(string guessedWord)
        {
            bool ok = false;
            string soundexOfGuessedWord = soundexEN.soundex(guessedWord);
            if (!alreadyGuessedWords.Contains(soundexOfGuessedWord))
            {
                alreadyGuessedWords.Add(soundexOfGuessedWord);     
                string output = guessedWord;
               
                foreach (Word word in words)
                {
                    if (soundexEN.DifferenceInSoudex(word.Title.ToLower(), soundexOfGuessedWord) >= 3)
                    {
                        ok = true;
                        guessedWords++;
                    }
                }
            }
            else
            {
                throw new Exception("You have already tried to answer this word \r\nor a similar word");
            }
            return ok;
        }

        public bool GuessedWordinListWithoutExceptionForDoubles(string guessedWord)
        {
            bool ok = false;
            string soundexOfGuessedWord = soundexEN.soundex(guessedWord);
            if (!alreadyGuessedWords.Contains(soundexOfGuessedWord))
            {
                alreadyGuessedWords.Add(soundexOfGuessedWord);
                string output = guessedWord;
                ok = false;
                foreach (Word word in words)
                {

                    if (soundexEN.DifferenceInSoudex(word.Title.ToLower(), soundexOfGuessedWord) >= 4)
                    {
                        ok = true;
                        guessedWords++;
                    }

                }
            }

            return ok;
        }

    }
}
