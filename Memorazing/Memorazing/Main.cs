﻿using Memorazing.Exercises;
using System;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using Memorazing.Models;
using System.Collections.Specialized;
using Memorazing.Controllers;
using Memorazing.WordsList;

namespace Memorazing
{
    public partial class Main : Form
    {
        private MainController mainController = new MainController(); 

<<<<<<< HEAD
        public Dictionary<string, List<Word>> DictioWords
=======
        public static Dictionary<string, List<Word>> DictioWords
>>>>>>> 53fb27240ae63966d53c5318a3b582e572cec6b1
        {
            get { return MainController.DictioWords; }
            set { MainController.DictioWords = value; }
        }
        
        public Main()
        {
            InitializeComponent();
            //.Text = string.Format("{0}      {1}", Options.NrOfWords, Options.WordSpeed);
        }

        private void OptionBtn_Click(object sender, EventArgs e)
        {
            OptionsForm optionsForm = new OptionsForm();
            optionsForm.Show();
        }

        private void Exercise1Btn_Click(object sender, EventArgs e)
        {
            Exercise1Controller exercise1 = new Exercise1Controller();
            exercise1.ShowWindow();
        }

        private void Exercise2Btn_Click(object sender, EventArgs e)
        {
            Exercise2Controller exercise2 = new Exercise2Controller();
            exercise2.ShowWindow();
        }

        private void Exercise3Btn_Click(object sender, EventArgs e)
        {
            Exercise3Controller exercise3 = new Exercise3Controller();
            exercise3.ShowWindow();
        }

        private void Exercise4Btn_Click(object sender, EventArgs e)
        {
            Exercise1Controller exercise4 = new Exercise1Controller();
            exercise4.ShowWindow();
        }

        private void Exercise5Btn_Click(object sender, EventArgs e)
        {
            Exercise1Controller exercise5 = new Exercise1Controller();
            exercise5.ShowWindow();
        }

        private void Exercise6Btn_Click(object sender, EventArgs e)
        {
            Exercise6Form exercise6 = new Exercise6Form();
            exercise6.Show();
        }

        private void btnAddList_Click(object sender, EventArgs e)
        {
            ListForms listforms = new ListForms();
            listforms.Show();
        }

        private void btnModifyList_Click(object sender, EventArgs e)
        {
            ModifyListController modifyList = new ModifyListController();
            modifyList.Show();
        }

        public static void DeleteWord()
        {
            ModifyListController modifyList = new ModifyListController();
            modifyList.DeleteWord();
        }

        private void HistoryBtn_Click(object sender, EventArgs e)
        {
            HistoryForm historyForm = new HistoryForm();
            historyForm.Show();
        }

    }
}
