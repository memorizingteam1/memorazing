﻿namespace Memorazing
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.OptionBtn = new System.Windows.Forms.Button();
            this.Exercice1Lbl = new System.Windows.Forms.Label();
            this.Exercice2Lbl = new System.Windows.Forms.Label();
            this.Exercice3Lbl = new System.Windows.Forms.Label();
            this.Exercice4Lbl = new System.Windows.Forms.Label();
            this.Exercice5Lbl = new System.Windows.Forms.Label();
            this.Exercice6Lbl = new System.Windows.Forms.Label();
            this.Exercise1Btn = new System.Windows.Forms.Button();
            this.Exercise2Btn = new System.Windows.Forms.Button();
            this.Exercise3Btn = new System.Windows.Forms.Button();
            this.Exercise5Btn = new System.Windows.Forms.Button();
            this.Exercise4Btn = new System.Windows.Forms.Button();
            this.Exercise6Btn = new System.Windows.Forms.Button();
            this.btnAddList = new System.Windows.Forms.Button();
            this.btnModifyList = new System.Windows.Forms.Button();
            this.HistoryBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OptionBtn
            // 
            this.OptionBtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.OptionBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionBtn.ForeColor = System.Drawing.Color.White;
            this.OptionBtn.Location = new System.Drawing.Point(56, 459);
            this.OptionBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OptionBtn.Name = "OptionBtn";
            this.OptionBtn.Size = new System.Drawing.Size(143, 52);
            this.OptionBtn.TabIndex = 0;
            this.OptionBtn.Text = "Options";
            this.OptionBtn.UseVisualStyleBackColor = false;
            this.OptionBtn.Click += new System.EventHandler(this.OptionBtn_Click);
            // 
            // Exercice1Lbl
            // 
            this.Exercice1Lbl.AutoSize = true;
            this.Exercice1Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice1Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice1Lbl.ForeColor = System.Drawing.Color.Crimson;
            this.Exercice1Lbl.Location = new System.Drawing.Point(41, 32);
            this.Exercice1Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice1Lbl.Name = "Exercice1Lbl";
            this.Exercice1Lbl.Size = new System.Drawing.Size(474, 31);
            this.Exercice1Lbl.TabIndex = 1;
            this.Exercice1Lbl.Text = "Exercice 1 - Mémoire visuelle / linéaire";
            // 
            // Exercice2Lbl
            // 
            this.Exercice2Lbl.AutoSize = true;
            this.Exercice2Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice2Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice2Lbl.ForeColor = System.Drawing.Color.Orange;
            this.Exercice2Lbl.Location = new System.Drawing.Point(41, 106);
            this.Exercice2Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice2Lbl.Name = "Exercice2Lbl";
            this.Exercice2Lbl.Size = new System.Drawing.Size(551, 31);
            this.Exercice2Lbl.TabIndex = 3;
            this.Exercice2Lbl.Text = "Exercice 2 - Mémoire visuelle / vision globale";
            // 
            // Exercice3Lbl
            // 
            this.Exercice3Lbl.AutoSize = true;
            this.Exercice3Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice3Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice3Lbl.ForeColor = System.Drawing.Color.Goldenrod;
            this.Exercice3Lbl.Location = new System.Drawing.Point(41, 180);
            this.Exercice3Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice3Lbl.Name = "Exercice3Lbl";
            this.Exercice3Lbl.Size = new System.Drawing.Size(477, 31);
            this.Exercice3Lbl.TabIndex = 4;
            this.Exercice3Lbl.Text = "Exercice 3 - Mémoire auditive / linéaire";
            // 
            // Exercice4Lbl
            // 
            this.Exercice4Lbl.AutoSize = true;
            this.Exercice4Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice4Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice4Lbl.ForeColor = System.Drawing.Color.ForestGreen;
            this.Exercice4Lbl.Location = new System.Drawing.Point(41, 254);
            this.Exercice4Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice4Lbl.Name = "Exercice4Lbl";
            this.Exercice4Lbl.Size = new System.Drawing.Size(596, 31);
            this.Exercice4Lbl.TabIndex = 5;
            this.Exercice4Lbl.Text = "Exercice 4 - Mémoire associative / vision globale";
            // 
            // Exercice5Lbl
            // 
            this.Exercice5Lbl.AutoSize = true;
            this.Exercice5Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice5Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice5Lbl.ForeColor = System.Drawing.Color.MidnightBlue;
            this.Exercice5Lbl.Location = new System.Drawing.Point(41, 327);
            this.Exercice5Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice5Lbl.Name = "Exercice5Lbl";
            this.Exercice5Lbl.Size = new System.Drawing.Size(441, 31);
            this.Exercice5Lbl.TabIndex = 6;
            this.Exercice5Lbl.Text = "Exercice 5 - Mémoire kinesthésique";
            // 
            // Exercice6Lbl
            // 
            this.Exercice6Lbl.AutoSize = true;
            this.Exercice6Lbl.BackColor = System.Drawing.Color.Transparent;
            this.Exercice6Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercice6Lbl.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Exercice6Lbl.Location = new System.Drawing.Point(41, 401);
            this.Exercice6Lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice6Lbl.Name = "Exercice6Lbl";
            this.Exercice6Lbl.Size = new System.Drawing.Size(374, 31);
            this.Exercice6Lbl.TabIndex = 7;
            this.Exercice6Lbl.Text = "Exercice 6 - Créativité et sens";
            // 
            // Exercise1Btn
            // 
            this.Exercise1Btn.BackColor = System.Drawing.Color.Crimson;
            this.Exercise1Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise1Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise1Btn.Location = new System.Drawing.Point(708, 25);
            this.Exercise1Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise1Btn.Name = "Exercise1Btn";
            this.Exercise1Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise1Btn.TabIndex = 8;
            this.Exercise1Btn.Text = "Start";
            this.Exercise1Btn.UseVisualStyleBackColor = false;
            this.Exercise1Btn.Click += new System.EventHandler(this.Exercise1Btn_Click);
            // 
            // Exercise2Btn
            // 
            this.Exercise2Btn.BackColor = System.Drawing.Color.Orange;
            this.Exercise2Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise2Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise2Btn.Location = new System.Drawing.Point(708, 98);
            this.Exercise2Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise2Btn.Name = "Exercise2Btn";
            this.Exercise2Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise2Btn.TabIndex = 9;
            this.Exercise2Btn.Text = "Start";
            this.Exercise2Btn.UseVisualStyleBackColor = false;
            this.Exercise2Btn.Click += new System.EventHandler(this.Exercise2Btn_Click);
            // 
            // Exercise3Btn
            // 
            this.Exercise3Btn.BackColor = System.Drawing.Color.Goldenrod;
            this.Exercise3Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise3Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise3Btn.Location = new System.Drawing.Point(708, 172);
            this.Exercise3Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise3Btn.Name = "Exercise3Btn";
            this.Exercise3Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise3Btn.TabIndex = 10;
            this.Exercise3Btn.Text = "Start";
            this.Exercise3Btn.UseVisualStyleBackColor = false;
            this.Exercise3Btn.Click += new System.EventHandler(this.Exercise3Btn_Click);
            // 
            // Exercise5Btn
            // 
            this.Exercise5Btn.BackColor = System.Drawing.Color.MidnightBlue;
            this.Exercise5Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise5Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise5Btn.Location = new System.Drawing.Point(708, 320);
            this.Exercise5Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise5Btn.Name = "Exercise5Btn";
            this.Exercise5Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise5Btn.TabIndex = 11;
            this.Exercise5Btn.Text = "Start";
            this.Exercise5Btn.UseVisualStyleBackColor = false;
            this.Exercise5Btn.Click += new System.EventHandler(this.Exercise5Btn_Click);
            // 
            // Exercise4Btn
            // 
            this.Exercise4Btn.BackColor = System.Drawing.Color.ForestGreen;
            this.Exercise4Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise4Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise4Btn.Location = new System.Drawing.Point(708, 246);
            this.Exercise4Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise4Btn.Name = "Exercise4Btn";
            this.Exercise4Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise4Btn.TabIndex = 12;
            this.Exercise4Btn.Text = "Start";
            this.Exercise4Btn.UseVisualStyleBackColor = false;
            this.Exercise4Btn.Click += new System.EventHandler(this.Exercise4Btn_Click);
            // 
            // Exercise6Btn
            // 
            this.Exercise6Btn.BackColor = System.Drawing.Color.DarkMagenta;
            this.Exercise6Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exercise6Btn.ForeColor = System.Drawing.Color.White;
            this.Exercise6Btn.Location = new System.Drawing.Point(708, 394);
            this.Exercise6Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Exercise6Btn.Name = "Exercise6Btn";
            this.Exercise6Btn.Size = new System.Drawing.Size(143, 49);
            this.Exercise6Btn.TabIndex = 13;
            this.Exercise6Btn.Text = "Start";
            this.Exercise6Btn.UseVisualStyleBackColor = false;
            this.Exercise6Btn.Click += new System.EventHandler(this.Exercise6Btn_Click);
            // 
            // btnAddList
            // 
            this.btnAddList.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnAddList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddList.ForeColor = System.Drawing.Color.White;
            this.btnAddList.Location = new System.Drawing.Point(537, 459);
            this.btnAddList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddList.Name = "btnAddList";
            this.btnAddList.Size = new System.Drawing.Size(143, 52);
            this.btnAddList.TabIndex = 14;
            this.btnAddList.Text = "AddList";
            this.btnAddList.UseVisualStyleBackColor = false;
            this.btnAddList.Click += new System.EventHandler(this.btnAddList_Click);
            // 
            // btnModifyList
            // 
            this.btnModifyList.BackColor = System.Drawing.Color.White;
            this.btnModifyList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyList.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnModifyList.Location = new System.Drawing.Point(708, 459);
            this.btnModifyList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnModifyList.Name = "btnModifyList";
            this.btnModifyList.Size = new System.Drawing.Size(143, 52);
            this.btnModifyList.TabIndex = 15;
            this.btnModifyList.Text = "ModifyList";
            this.btnModifyList.UseVisualStyleBackColor = false;
            this.btnModifyList.Click += new System.EventHandler(this.btnModifyList_Click);
            // 
            // HistoryBtn
            // 
            this.HistoryBtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.HistoryBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HistoryBtn.ForeColor = System.Drawing.Color.White;
            this.HistoryBtn.Location = new System.Drawing.Point(296, 459);
            this.HistoryBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HistoryBtn.Name = "HistoryBtn";
            this.HistoryBtn.Size = new System.Drawing.Size(143, 52);
            this.HistoryBtn.TabIndex = 16;
            this.HistoryBtn.Text = "History";
            this.HistoryBtn.UseVisualStyleBackColor = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(909, 522);
            this.Controls.Add(this.HistoryBtn);
            this.Controls.Add(this.btnModifyList);
            this.Controls.Add(this.btnAddList);
            this.Controls.Add(this.Exercise6Btn);
            this.Controls.Add(this.Exercise4Btn);
            this.Controls.Add(this.Exercise5Btn);
            this.Controls.Add(this.Exercise3Btn);
            this.Controls.Add(this.Exercise2Btn);
            this.Controls.Add(this.Exercise1Btn);
            this.Controls.Add(this.Exercice6Lbl);
            this.Controls.Add(this.Exercice5Lbl);
            this.Controls.Add(this.Exercice4Lbl);
            this.Controls.Add(this.Exercice3Lbl);
            this.Controls.Add(this.Exercice2Lbl);
            this.Controls.Add(this.Exercice1Lbl);
            this.Controls.Add(this.OptionBtn);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Main";
            this.Text = "SWAG, Secretly We Are Genius";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OptionBtn;
        private System.Windows.Forms.Label Exercice2Lbl;
        private System.Windows.Forms.Label Exercice3Lbl;
        private System.Windows.Forms.Label Exercice4Lbl;
        private System.Windows.Forms.Label Exercice5Lbl;
        private System.Windows.Forms.Label Exercice6Lbl;
        private System.Windows.Forms.Button Exercise1Btn;
        private System.Windows.Forms.Button Exercise2Btn;
        private System.Windows.Forms.Button Exercise3Btn;
        private System.Windows.Forms.Button Exercise5Btn;
        private System.Windows.Forms.Button Exercise4Btn;
        private System.Windows.Forms.Button Exercise6Btn;
        private System.Windows.Forms.Label Exercice1Lbl;
        private System.Windows.Forms.Button btnAddList;
        private System.Windows.Forms.Button btnModifyList;
        private System.Windows.Forms.Button HistoryBtn;
    }
}

