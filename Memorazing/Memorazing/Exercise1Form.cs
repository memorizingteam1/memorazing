﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Memorazing
{

    public partial class OutputLabel : Form
    {
        List<String> words = new List<string>()
        {
            "test",
            "test2",
            "test3",
            "test4"
        };
        private int guessesMade = 0;
        private int guessedWords = 0;
        private List<String> alreadyGuessedWords = new List<string>();

        public OutputLabel()
        {
            InitializeComponent();
            StartShowingAllTheWords();
        }

        private async void StartShowingAllTheWords()
        {
            this.Update();
            foreach (string s in words)
            {
                await Task.Delay(Options.WordSpeed * 1000);
                ShowWordLbl.Text += s + " \r\n";
                this.Update();
            }
            await Task.Delay(Options.WordSpeed * 1000);
            ShowWordLbl.Text = "";
            SubmitWordBtn.Visible = true;
            InputWordTextBox.Visible = true;
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SubmitWordBtn_Click(object sender, EventArgs e)
        {
            string guessedWord = InputWordTextBox.Text.TrimStart().TrimEnd();
            if (!alreadyGuessedWords.Contains(guessedWord))
            {
                alreadyGuessedWords.Add(guessedWord);
                guessesMade++;
                ErrorLabel.Text = "";
                string output = guessedWord;
                if (words.Contains(guessedWord))
                {
                    output += " ----- O";
                    guessedWords++;           
                }
                else
                {
                    output += " ----- X";
                }
                guessedWordsLabel.Text += output + " \r\n";
                if (guessesMade >= Options.NrOfWords) endForm();
            }
            else ErrorLabel.Text = "You have already tried to answer this word";
        }

        private void endForm()
        {
            InputWordTextBox.Enabled = false;
            SubmitWordBtn.Enabled = false;
            resultLabel.Text = string.Format("You have guessed {0} out of {1} words", guessedWords, Options.NrOfWords);
        }
        #region soundex

        private string soundex(string data)
        {
            StringBuilder result = new StringBuilder();

            if (data != null && data.Length > 0)
            {
                string previousCode = "";
                string currentCode = "";
                string currentLetter = "";

                result.Append(data.Substring(0, 1));

                for (int i = 1; i < data.Length; i++)
                {
                    currentLetter = data.Substring(i, 1).ToLower();
                    currentCode = "";

                    if ("bpfv".IndexOf(currentLetter) > -1)
                        currentCode = "1";
                    else if ("cgjkqsxz".IndexOf(currentLetter) > -1)
                        currentCode = "2";
                    else if ("dt".IndexOf(currentLetter) > -1)
                        currentCode = 3;
                    else if (currentLetter == "l")
                        currentCode = 4;
                    else if ("mn".IndexOf(currentLetter) > -1)
                        currentCode = 5;
                    else if (currentLetter == "r")
                        currentCode = 6;

                    if (currentCode != previousCode)
                        result.Append(currentCode);
                    if (result.Length == 4) break;

                    if (currentCode != "")
                        previousCode = currentCode;

                }
            }
            if (result.Length < 4)
                result.Append(new String('0', 4 - result.Length));
            return result.ToString().ToUpper();
        }

        private int DifferenceInSoudex(string data1, string data2)
        {
            int result = 0;
            string soundex1 = soundex(data1);
            string soundex2 = soundex(data2);

            if (soundex1 == sundex2)
                result = 4;
            else
            {
                string sub1 = soundex1.Substring(1, 3);
                string sub2 = soundex1.Substring(2, 2);
                string sub3 = soundex1.Substring(1, 2);
                string sub4 = soundex1.Substring(1, 1);
                string sub5 = soundex1.Substring(2, 1);
                string sub6 = soundex1.Substring(3, 1);

                if (soundex2.IndexOf(sub1) > -1)
                {
                    result = 3;
                }
                else if (soundex2.IndexOf(sub2) > -1)
                    result = 2;
                else if (soundex2.IndexOf(sub3) > -1)
                {
                    result = 2;
                }
                else
                {
                    if (soundex2.IndexOf(sub4) > -1) result++;
                    if (soundex2.IndexOf(sub5) > -1) result++;
                    if (soundex2.IndexOf(sub6) > -1) result++;
                }
                if (soundex1.Substring(0, 1) == soundex2.Substring(0, 1)) result++;
            }

            return (result == 0) ? 1 : result;

        }

        #endregion

    }
}
