﻿namespace Memorazing
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.NumberOfWordsNum = new System.Windows.Forms.NumericUpDown();
            this.TimePerWordNum = new System.Windows.Forms.NumericUpDown();
            this.LblForNrOfWords = new System.Windows.Forms.Label();
            this.LblForTimePerWord = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfWordsNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimePerWordNum)).BeginInit();
            this.SuspendLayout();
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.Location = new System.Drawing.Point(48, 186);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(75, 23);
            this.SubmitBtn.TabIndex = 0;
            this.SubmitBtn.Text = "Submit";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(240, 186);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 1;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // NumberOfWordsNum
            // 
            this.NumberOfWordsNum.Location = new System.Drawing.Point(240, 64);
            this.NumberOfWordsNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NumberOfWordsNum.Name = "NumberOfWordsNum";
            this.NumberOfWordsNum.Size = new System.Drawing.Size(120, 22);
            this.NumberOfWordsNum.TabIndex = 2;
            // 
            // TimePerWordNum
            // 
            this.TimePerWordNum.Location = new System.Drawing.Point(240, 110);
            this.TimePerWordNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.TimePerWordNum.Name = "TimePerWordNum";
            this.TimePerWordNum.Size = new System.Drawing.Size(120, 22);
            this.TimePerWordNum.TabIndex = 3;
            // 
            // LblForNrOfWords
            // 
            this.LblForNrOfWords.AutoSize = true;
            this.LblForNrOfWords.Location = new System.Drawing.Point(45, 64);
            this.LblForNrOfWords.Name = "LblForNrOfWords";
            this.LblForNrOfWords.Size = new System.Drawing.Size(115, 17);
            this.LblForNrOfWords.TabIndex = 4;
            this.LblForNrOfWords.Text = "Number of words";
            // 
            // LblForTimePerWord
            // 
            this.LblForTimePerWord.AutoSize = true;
            this.LblForTimePerWord.Location = new System.Drawing.Point(45, 110);
            this.LblForTimePerWord.Name = "LblForTimePerWord";
            this.LblForTimePerWord.Size = new System.Drawing.Size(180, 17);
            this.LblForTimePerWord.TabIndex = 5;
            this.LblForTimePerWord.Text = "Time per word (in seconds)";
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 342);
            this.Controls.Add(this.LblForTimePerWord);
            this.Controls.Add(this.LblForNrOfWords);
            this.Controls.Add(this.TimePerWordNum);
            this.Controls.Add(this.NumberOfWordsNum);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.SubmitBtn);
            this.Name = "OptionsForm";
            this.Text = "OptionsForm";
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfWordsNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimePerWordNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.NumericUpDown NumberOfWordsNum;
        private System.Windows.Forms.NumericUpDown TimePerWordNum;
        private System.Windows.Forms.Label LblForNrOfWords;
        private System.Windows.Forms.Label LblForTimePerWord;
    }
}