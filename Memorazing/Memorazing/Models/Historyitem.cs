﻿using System;

namespace Memorazing.Models
{
    [Serializable]
    public class HistoryItem
    {
        public DateTime TimeWhenTheExerciceIsDone { get; set; }

        public int NbrOfWordsGuessed { get; set; }

        public int NbrOfWordsInList { get; set; }

        public int ExerciseNumber { get; set; }

        public HistoryItem(int nrOfWordsGuessed, int nrOfWordsInList, int exerciseNumber)
        {
            ExerciseNumber = exerciseNumber;
            NbrOfWordsGuessed = nrOfWordsGuessed;
            NbrOfWordsInList = nrOfWordsInList;
            TimeWhenTheExerciceIsDone = DateTime.Now;
        }

    }
}