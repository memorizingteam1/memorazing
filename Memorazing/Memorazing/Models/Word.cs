﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memorazing.Models
{
    [Serializable]
    public class Word 
    {
        public Word(string title)
        {
            this.Title = title;
        }

        public Word(string title, string UrlImage)
        {
            this.Title = title;
            this.UrlPictures = UrlImage;
        }

        public string Title { get; set; }

        public string UrlPictures { get; set; }

        //public string UrlSound { get; set; }

        public string WordAssociation { get; set; }


    }
}
