﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Memorazing.Models
{
    [Serializable]
    static class History
    {

        public static List<HistoryItem> history = new List<HistoryItem>();

        private static void SaveHistory()
        {
            try
            {
                string path = Environment.CurrentDirectory + @"\history.txt";
                FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write);

                //serialiseren
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, history);
                stream.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public static void LoadHistory()
        {
            try
            {
                string path = Environment.CurrentDirectory + @"\history.txt";
                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    List<HistoryItem> historyFromFile = (List<HistoryItem>)formatter.Deserialize(stream);
                    if (historyFromFile != null || historyFromFile.Count != 0)
                    {
                        history = historyFromFile;
                    }
                }
                catch (Exception e) { }

                stream.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static void addHistoryItem(HistoryItem historyItem)
        {
            history.Add(historyItem);
            SaveHistory();
        }
    }
}
