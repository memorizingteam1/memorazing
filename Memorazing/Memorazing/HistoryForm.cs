﻿using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing
{
    public partial class HistoryForm : Form
    {
        public HistoryForm()
        {
            InitializeComponent();
            writeOutHistory();
        }

        private void writeOutHistory()
        {

            foreach (HistoryItem item in History.history)
            {
                string output = string.Format("Exercise {0} was done on: {1} and the result was {2} out of {3}", item.ExerciseNumber, item.TimeWhenTheExerciceIsDone, item.NbrOfWordsGuessed, item.NbrOfWordsInList);
                outputLabel.Text += output + " \r\n";
            }

        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
