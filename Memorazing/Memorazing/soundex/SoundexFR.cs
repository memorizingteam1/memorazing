﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memorazing.soundex
{
    static class SoundexFR
    {
        private static string soundex(string data)
        {
            StringBuilder result = new StringBuilder();

            if (data != null && data.Length > 0)
            {
                string previousCode = "";
                string currentCode = "";
                string currentLetter = "";

                result.Append(data.Substring(0, 1));

                for (int i = 1; i < data.Length; i++)
                {
                    currentLetter = data.Substring(i, 1).ToLower();
                    currentCode = "";

                    if ("bp".IndexOf(currentLetter) > -1)
                        currentCode = "1";
                    else if ("ckq".IndexOf(currentLetter) > -1)
                        currentCode = "2";
                    else if ("dt".IndexOf(currentLetter) > -1)
                        currentCode = "3";
                    else if (currentLetter == "l")
                        currentCode = "4";
                    else if ("mn".IndexOf(currentLetter) > -1)
                        currentCode = "5";
                    else if (currentLetter == "r")
                        currentCode = "6";
                    else if ("gj".IndexOf(currentLetter) > -1)
                        currentCode = "7";
                    else if ("xzs".IndexOf(currentLetter) > -1)
                        currentCode = "8";
                    else if ("fv".IndexOf(currentLetter) > -1)
                        currentCode = "9";


                    if (currentCode != previousCode && !("aehiouwy".IndexOf(currentLetter) == -1))
                        result.Append(currentCode);
                    if (result.Length == 4) break;

                    if (currentCode != "")
                        previousCode = currentCode;

                }
            }
            if (result.Length < 4)
                result.Append(new String('0', 4 - result.Length));
            return result.ToString().ToUpper();
        }

        private static int DifferenceInSoudex(string data1, string SoundexOfGuessedWord)
        {
            int result = 0;
            string soundex1 = soundex(data1);
            string soundex2 = SoundexOfGuessedWord;

            if (soundex1 == soundex2)
                result = 4;
            else
            {
                string sub1 = soundex1.Substring(1, 3);
                string sub2 = soundex1.Substring(2, 2);
                string sub3 = soundex1.Substring(1, 2);
                string sub4 = soundex1.Substring(1, 1);
                string sub5 = soundex1.Substring(2, 1);
                string sub6 = soundex1.Substring(3, 1);

                if (soundex2.IndexOf(sub1) > -1)
                {
                    result = 3;
                }
                else if (soundex2.IndexOf(sub2) > -1)
                    result = 2;
                else if (soundex2.IndexOf(sub3) > -1)
                {
                    result = 2;
                }
                else
                {
                    if (soundex2.IndexOf(sub4) > -1) result++;
                    if (soundex2.IndexOf(sub5) > -1) result++;
                    if (soundex2.IndexOf(sub6) > -1) result++;
                }
                if (soundex1.Substring(0, 1) == soundex2.Substring(0, 1)) result++;
            }

            return (result == 0) ? 1 : result;

        }


    }
}
