﻿namespace Memorazing.WordsList
{
    partial class ModifyListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_deleteList = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_saveChange = new System.Windows.Forms.Button();
            this.btn_deleteRow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 98);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(421, 172);
            this.dataGridView1.TabIndex = 1;
            // 
            // btn_deleteList
            // 
            this.btn_deleteList.Location = new System.Drawing.Point(33, 277);
            this.btn_deleteList.Name = "btn_deleteList";
            this.btn_deleteList.Size = new System.Drawing.Size(75, 23);
            this.btn_deleteList.TabIndex = 2;
            this.btn_deleteList.Text = "Delete List";
            this.btn_deleteList.UseVisualStyleBackColor = true;
            this.btn_deleteList.Click += new System.EventHandler(this.btn_deleteList_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(358, 277);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 3;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_saveChange
            // 
            this.btn_saveChange.Location = new System.Drawing.Point(228, 277);
            this.btn_saveChange.Name = "btn_saveChange";
            this.btn_saveChange.Size = new System.Drawing.Size(90, 23);
            this.btn_saveChange.TabIndex = 4;
            this.btn_saveChange.Text = "Save changes";
            this.btn_saveChange.UseVisualStyleBackColor = true;
            this.btn_saveChange.Click += new System.EventHandler(this.btn_saveChange_Click);
            // 
            // btn_deleteRow
            // 
            this.btn_deleteRow.Location = new System.Drawing.Point(133, 277);
            this.btn_deleteRow.Name = "btn_deleteRow";
            this.btn_deleteRow.Size = new System.Drawing.Size(75, 23);
            this.btn_deleteRow.TabIndex = 5;
            this.btn_deleteRow.Text = "Delete row";
            this.btn_deleteRow.UseVisualStyleBackColor = true;
            this.btn_deleteRow.Click += new System.EventHandler(this.btn_deleteRow_Click);
            // 
            // ModifyListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 312);
            this.Controls.Add(this.btn_deleteRow);
            this.Controls.Add(this.btn_saveChange);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_deleteList);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Name = "ModifyListForm";
            this.Text = "ModifyListForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_deleteList;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_saveChange;
        private System.Windows.Forms.Button btn_deleteRow;
    }
}