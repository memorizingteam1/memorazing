﻿using Memorazing.Controllers;
using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing
{
    public partial class ListForms : Form
    {
        public ListForms()
        {
            InitializeComponent();
        }

        OpenFileDialog ofd = new OpenFileDialog();

        List<Word> ListWords = new List<Word>();

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnAddWordToList_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != null && textBox3.Text == null)
            {
                ListWords.Add(new Word(textBox2.Text));
            }
            else if (textBox2.Text != null && textBox3.Text != null)
            {
                ListWords.Add(new Word(textBox2.Text, @"..\..\Pictures\" + textBox4.Text));

                System.IO.File.Copy(textBox3.Text, @"..\..\Pictures\" + textBox4.Text, true);
            }
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
        }

        private void btnValidateList_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != null && ! MainController.DictioWords.Keys.Contains(textBox1.Text))
            {
                MainController.DictioWords.Add(textBox1.Text, ListWords);
                ExportListToFile();
                this.Close();
            }else if(textBox1.Text != null && MainController.DictioWords.Keys.Contains(textBox1.Text))
            {
                foreach (Word w in ListWords)
                {
                    if (!MainController.DictioWords[textBox1.Text].Contains(w))
                    {
                        MainController.DictioWords[textBox1.Text].Add(w);
                    }
                }                
                ExportListToFile();
                this.Close();
            }
        }

        public static void ExportListToFile()
        {
            string path = Environment.CurrentDirectory + @"\listOfWords.txt";
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                //serialiser
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, MainController.DictioWords);
            }
        }

        private void btn_openFile_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = ofd.FileName;
                textBox4.Text = ofd.SafeFileName;
            }
        }
    }
}
