﻿using Memorazing.Controllers;
using Memorazing.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memorazing.WordsList
{
    public partial class ModifyListForm : Form
    {
        public ModifyListForm()
        {
            InitializeComponent();
<<<<<<< HEAD
=======
            int i = 0;

            foreach (string s in MainController.DictioWords.Keys)
            {
                /*label1.Text += s;
                label1.Visible = false;*/
                Button button = creationBoutons(s, i);
                i += 50;

                /*foreach (Word w  in Main.DictioWords[s])
                {
                    label1.Text += w.Title + " ";
                    label1.Text += w.UrlPictures + " ";
                    label1.Text += "-----\n";
                }*/
            }
>>>>>>> 53fb27240ae63966d53c5318a3b582e572cec6b1
        }

        public Button CreateButtons(String textBouton, int locationBouton)
        {
            Button bouton = new Button();
            bouton.Name = textBouton + locationBouton;
            bouton.Text = textBouton;
            bouton.Location = new Point(150, locationBouton);
            bouton.Size = new Size(100, 28);
            bouton.Click += Bouton_Click;
            this.Controls.Add(bouton);
            return bouton;
        }

        private void Bouton_Click(object sender, EventArgs e)
        {
            /*Button bouton = (Button)sender;
            foreach (string s in Main.DictioWords.Keys)
            {
                label1.Text = null;
                if (bouton.Text.Equals(s))
                {
                    foreach (Word w in Main.DictioWords[s])
                    {
                        label1.Text += w.Title + " ";
                        label1.Text += w.UrlPictures + " ";
                        label1.Text += "-----\n";
                    }
                    this.Refresh();
                    break;
                }
            }*/
            Button bouton = (Button)sender;
            List<Word> listOfWordsFromDictio = MainController.DictioWords[bouton.Text];
            label1.Text = bouton.Text;
            label1.Visible = false;
            dataGridView1.DataSource = listOfWordsFromDictio;
        }

        private void btn_deleteList_Click(object sender, EventArgs e)
        {
            Main.DeleteWord();
        }

        public string Label1Text()
        {
            return label1.Text;
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_saveChange_Click(object sender, EventArgs e)
        { 
            //Il faudrait réussir à connaître le chemin avant qu'il soit définitivement supprimé.

            //Main.DictioWords[label1.Text] = (List<Word>)dataGridView1.DataSource;
            //foreach (Word w in Main.DictioWords[label1.Text])
            foreach(Word w in (List<Word>)dataGridView1.DataSource)
            {
                if(w.Title == null && w.UrlPictures == null)
                {
                    //if(Main.DictioWords[label1.Text]. .Equals())
                    //File.Delete(w.UrlPictures);
                    MainController.DictioWords[label1.Text].Remove(w);
                    this.Close();
                }
                else if(w.Title == null)
                {
                    MessageBox.Show("Vous devez rentré un nom");
                }else if(w.UrlPictures == null)
                {
                    MessageBox.Show("Vous devez rentré un chemin vers l'image");
                }
            }
            MainController.DictioWords[label1.Text] = (List<Word>)dataGridView1.DataSource;
            ListForms.ExportListToFile();
        }

        private void btn_deleteRow_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(item.Index);
            }
            MainController.DictioWords[label1.Text] = (List<Word>)dataGridView1.DataSource;
            ListForms.ExportListToFile();
            //this.Close();
        }
    }
}
