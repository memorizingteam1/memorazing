﻿namespace Memorazing
{
    partial class OutputLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShowWordLbl = new System.Windows.Forms.Label();
            this.InputWordTextBox = new System.Windows.Forms.TextBox();
            this.SubmitWordBtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.guessedWordsLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ShowWordLbl
            // 
            this.ShowWordLbl.AutoSize = true;
            this.ShowWordLbl.Location = new System.Drawing.Point(31, 31);
            this.ShowWordLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ShowWordLbl.Name = "ShowWordLbl";
            this.ShowWordLbl.Size = new System.Drawing.Size(0, 13);
            this.ShowWordLbl.TabIndex = 0;
            // 
            // InputWordTextBox
            // 
            this.InputWordTextBox.Location = new System.Drawing.Point(169, 234);
            this.InputWordTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.InputWordTextBox.Name = "InputWordTextBox";
            this.InputWordTextBox.Size = new System.Drawing.Size(76, 20);
            this.InputWordTextBox.TabIndex = 1;
            this.InputWordTextBox.Visible = false;
            // 
            // SubmitWordBtn
            // 
            this.SubmitWordBtn.Location = new System.Drawing.Point(290, 233);
            this.SubmitWordBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SubmitWordBtn.Name = "SubmitWordBtn";
            this.SubmitWordBtn.Size = new System.Drawing.Size(56, 19);
            this.SubmitWordBtn.TabIndex = 2;
            this.SubmitWordBtn.Text = "Submit";
            this.SubmitWordBtn.UseVisualStyleBackColor = true;
            this.SubmitWordBtn.Visible = false;
            this.SubmitWordBtn.Click += new System.EventHandler(this.SubmitWordBtn_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(482, 282);
            this.ExitBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(56, 19);
            this.ExitBtn.TabIndex = 3;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // guessedWordsLabel
            // 
            this.guessedWordsLabel.AutoSize = true;
            this.guessedWordsLabel.Location = new System.Drawing.Point(428, 31);
            this.guessedWordsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.guessedWordsLabel.Name = "guessedWordsLabel";
            this.guessedWordsLabel.Size = new System.Drawing.Size(0, 13);
            this.guessedWordsLabel.TabIndex = 4;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new System.Drawing.Point(248, 114);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 13);
            this.resultLabel.TabIndex = 5;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.Location = new System.Drawing.Point(248, 50);
            this.ErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorLabel.TabIndex = 6;
            // 
            // OutputLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.guessedWordsLabel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SubmitWordBtn);
            this.Controls.Add(this.InputWordTextBox);
            this.Controls.Add(this.ShowWordLbl);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "OutputLabel";
            this.Text = "Exercise1Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ShowWordLbl;
        private System.Windows.Forms.TextBox InputWordTextBox;
        private System.Windows.Forms.Button SubmitWordBtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Label guessedWordsLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label ErrorLabel;
    }
}